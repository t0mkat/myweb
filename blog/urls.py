from django.urls import path
from . import views

urlpatterns = [
    path('blog/', views.blog, name='blog'),
    path('post/<int:blogpost_id>/', views.detail, name='detail'),
]
