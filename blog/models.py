from django.db import models
from django.utils import timezone


# This will be the model for a Blog Post which will contain Title, Publication Date,
# a Body of text and an image
class BlogPost(models.Model):
    title = models.CharField(max_length=256, unique=True)
    created_date = models.DateTimeField(default=timezone.now)
    pub_date = models.DateTimeField()
    body = models.TextField()
    image = models.ImageField(upload_to="images/")

    def __str__(self):
        return self.title

    def summary(self):
        return self.body[:100]

    def publish(self):
        self.pub_date = timezone.now()
        self.save()

    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y')
