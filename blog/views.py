from django.shortcuts import render, get_object_or_404
from .models import BlogPost

# Create your views here.
def blog(request):
    blogposts = BlogPost.objects
    return render(request, 'blog/blog.html', {'blogposts': blogposts})

def detail(request, blogpost_id):
    detail_blogpost = get_object_or_404(BlogPost, pk=blogpost_id)
    return render(request, 'blog/detail.html', {'blogpost': detail_blogpost})
